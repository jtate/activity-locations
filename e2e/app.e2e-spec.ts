import { ActivityLocationsPage } from './app.po';

describe('activity-locations App', () => {
  let page: ActivityLocationsPage;

  beforeEach(() => {
    page = new ActivityLocationsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { Component, OnInit } from '@angular/core';

import { Location } from '../location';
import { LocationService } from '../location.service';


@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {

  locations: Location[];
  selectedLocation: Location;
  addingLocation = false;
  error: any;
  showNgFor = false;

  constructor(
    private locationService: LocationService) { }

  getLocations(): void {
    this.locationService
      .getLocations()
      .then(locations => this.locations = locations)
      .catch(error => this.error = error);
  }

  addLocation(): void {
    console.debug('addLocation');
    this.addingLocation = true;
    this.selectedLocation = null;
  }

  close(savedLocation: Location): void {
    this.addingLocation = false;
    if (savedLocation) { this.getLocations(); }
  }

  deleteLocation(location: Location, event: any): void {
    event.stopPropagation();
    this.locationService
      .delete(location)
      .then(res => {
        this.locations = this.locations.filter(h => h !== location);
        if (this.selectedLocation === location) { this.selectedLocation = null; }
      })
      .catch(error => this.error = error);
  }

  ngOnInit(): void {
    this.getLocations();
  }

  onSelect(location: Location): void {
    console.debug(location);
    this.selectedLocation = location;
    this.addingLocation = false;
  }

}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Location } from '../location';
import { LocationService } from '../location.service';

@Component({
  selector: 'location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.css']
})
export class LocationDetailComponent implements OnInit {
  @Input() location: Location;
  @Output() close = new EventEmitter();
  error: any;

  constructor(
    private locationService: LocationService) {
  }

  ngOnInit(): void {
    if (!this.location) {
      this.location = new Location();
    }
  }

  save(): void {
    this.locationService
        .save(this.location)
        .then(location => {
          this.location = location; // saved hero, w/ id if new
        })
        .catch(error => this.error = error); // TODO: Display error message
  }

}
